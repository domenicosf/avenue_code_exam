package br.com.avenuecode.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.avenuecode.entity.Image;

public class ImageDAO {

	private final EntityManagerFactory entityManagerFactory;
	private final EntityManager entityManager;

	public ImageDAO() {
		/*
		 * Creating our EntityManagerFactory with the properties described in
		 * persistence.xml
		 */
		this.entityManagerFactory = Persistence.createEntityManagerFactory("persistence_unit_db_user");
		this.entityManager = this.entityManagerFactory.createEntityManager();
	}

	/*
	 * Save the Image Entity in the database
	 */
	public void save(Image imageEntity) {
		this.entityManager.getTransaction().begin();
		this.entityManager.persist(imageEntity);
		this.entityManager.getTransaction().commit();
	}

	/*
	 * Updating a existing ImageEntity
	 */
	public void update(Image imageEntity) {
		this.entityManager.getTransaction().begin();
		this.entityManager.merge(imageEntity);
		this.entityManager.getTransaction().commit();
	}

	/*
	 * Recovering all the Image entities excluding relationships
	 */
	public List<Image> getAllImage() {
		return this.entityManager.createNamedQuery("Image.findAllImage", Image.class).getResultList();
		//return this.entityManager.createQuery("from Image", Image.class).getResultList();
	}

	/*
	 * Recovering all Image entities including relationships
	 */
	public List<Image> getAllImageWithRelationship() {
		return this.entityManager.createNamedQuery("Image.findAllImageWithRelationship", Image.class).getResultList();
	}

	/*
	 * Recovering one Image based in primary key
	 */
	public Image getImageById(Long codigo) {
		return this.entityManager.createNamedQuery("Image.findImageById", Image.class).setParameter("codigo", codigo)
				.getSingleResult();
	}

	/*
	 * Removing one Image entity based on primary key
	 */
	public void deleteImageById(Long codigo) {
		Image imageEntity = this.getImageById(codigo);

		this.entityManager.getTransaction().begin();
		this.entityManager.remove(imageEntity);
		this.entityManager.getTransaction().commit();
	}

}
