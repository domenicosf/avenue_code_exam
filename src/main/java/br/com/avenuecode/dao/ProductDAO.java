package br.com.avenuecode.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.avenuecode.entity.Image;
import br.com.avenuecode.entity.Product;

public class ProductDAO {

	private final EntityManagerFactory entityManagerFactory;
	private final EntityManager entityManager;

	public ProductDAO() {
		/*
		 * Creating our EntityManagerFactory with the properties described in
		 * persistence.xml
		 */
		this.entityManagerFactory = Persistence.createEntityManagerFactory("persistence_unit_db_user");
		this.entityManager = this.entityManagerFactory.createEntityManager();
	}

	/*
	 * Save the Product Entity in the database
	 */
	public void save(Product productEntity) {
		this.entityManager.getTransaction().begin();
		this.entityManager.persist(productEntity);
		this.entityManager.getTransaction().commit();
	}

	/*
	 * Updating a existing ProductEntity
	 */
	public void update(Product productEntity) {
		this.entityManager.getTransaction().begin();
		this.entityManager.merge(productEntity);
		this.entityManager.getTransaction().commit();
	}

	/*
	 * Recovering all the Product entities excluding relationships
	 */
	public List<Product> getAllProduct() {
		return this.entityManager.createNamedQuery("Product.findAllProduct", Product.class).getResultList();
	}

	/*
	 * Recovering all Product entities including relationships
	 */
	public List<Product> getAllProductWithRelationship() {
		// return
		// this.entityManager.createNamedQuery("Product.findAllProductWithRelationship",
		// ProductRelations.class)
		// .getResultList();
		return this.entityManager.createNamedQuery("Product.findAllProductWithRelationship", Product.class)
				.getResultList();
	}

	/*
	 * Recovering one Product based in primary key
	 */
	public Product getProductById(Long codigo) {
		return this.entityManager.createNamedQuery("Product.findProductById", Product.class)
				.setParameter("codigo", codigo).getSingleResult();
	}

	/*
	 * Removing one Product entity based on primary key
	 */
	public void deleteProductById(Long codigo) {
		Product productEntity = this.getProductById(codigo);

		this.entityManager.getTransaction().begin();
		this.entityManager.remove(productEntity);
		this.entityManager.getTransaction().commit();
	}

	/*
	 * Recovering child products for specific product
	 */
	public List<Product> getChildrenByProductId(Long codigo) {
		return this.entityManager.createNamedQuery("Product.findChildrenByProductId", Product.class)
				.setParameter("codigo", codigo).getResultList();
	}

	/*
	 * Recovering specific product with its relationships
	 */
	public Product getProductByIdWithRelationship(Long codigo) {
		return this.entityManager.createNamedQuery("Product.findProductByIdWithRelationship", Product.class)
				.setParameter("codigo", codigo).getSingleResult();
	}

	/*
	 * Recovering a set of images based on product primary key
	 */
	public List<Image> getImagesByProductId(Long codigo) {
		return this.entityManager.createNamedQuery("Product.findImagesByProductId", Image.class)
				.setParameter("codigo", codigo).getResultList();
	}

}
