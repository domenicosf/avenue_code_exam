package br.com.avenuecode.http;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

public class ProductJSON {

	private Long id;
	private String name;
	private ProductJSON parent;
	private List<ProductJSON> children = new ArrayList<ProductJSON>();
	private List<ImageJSON> images = new ArrayList<ImageJSON>();

	public ProductJSON() {

	}

	public ProductJSON(Long id, String name, ProductJSON parent, List<ProductJSON> children, List<ImageJSON> images) {
		this.id = id;
		this.name = name;
		this.parent = parent;
		this.children = children;
		this.images = images;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@JsonManagedReference
	public ProductJSON getParent() {
		return parent;
	}

	public void setParent(ProductJSON parent) {
		this.parent = parent;
	}

	@JsonBackReference
	public List<ProductJSON> getChildren() {
		return children;
	}

	public void setChildren(List<ProductJSON> children) {
		this.children = children;
	}

	@JsonBackReference
	public List<ImageJSON> getImages() {
		return images;
	}

	public void setImages(List<ImageJSON> images) {
		this.images = images;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append("id= " + id + "\n");
		sb.append("name=" + name + "\n");

		if (parent != null) {
			sb.append("parent.id=" + parent.getId() + "\n");
			sb.append("parent.name=" + parent.getName() + "\n");
		}

		if ((children != null) && (children.size() > 0)) {
			for (ProductJSON child : children) {
				sb.append("child.id=" + child.getId() + "\n");
				sb.append("child.name=" + child.getName() + "\n");
			}
		}

		if ((images != null) && (images.size() > 0)) {
			for (ImageJSON image : images) {
				sb.append("image.id=" + image.getId() + "\n");
				sb.append("image.name=" + image.getDescription() + "\n");
			}
		}
		return sb.toString();
	}

}
