package br.com.avenuecode.http;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import br.com.avenuecode.entity.Product;

@XmlRootElement(name = "product")
public class ProductXml {

	private Long id;
	private String name;
	private Product parent;
	private List<ProductXml> children = new ArrayList<ProductXml>();
	private List<ImageXml> images = new ArrayList<ImageXml>();

	public ProductXml() {

	}

	public ProductXml(Long id, String name, Product parent, List<ProductXml> children, List<ImageXml> images) {
		this.id = id;
		this.name = name;
		this.parent = parent;
		this.children = children;
		this.images = images;
	}

	@XmlElement(name = "id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@XmlElement(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(name = "parent")
	public Product getParent() {
		return parent;
	}

	public void setParent(Product parent) {
		this.parent = parent;
	}

	// XmLElementWrapper generates a wrapper element around XML representation
	@XmlElementWrapper(name = "children")
	// XmlElement sets the name of the entities
	@XmlElement(name = "child")
	public List<ProductXml> getChildren() {
		return children;
	}

	public void setChildren(List<ProductXml> children) {
		this.children = children;
	}

	// XmLElementWrapper generates a wrapper element around XML representation
	@XmlElementWrapper(name = "images")
	// XmlElement sets the name of the entities
	@XmlElement(name = "image")
	public List<ImageXml> getImages() {
		return images;
	}

	public void setImages(List<ImageXml> images) {
		this.images = images;
	}

}
