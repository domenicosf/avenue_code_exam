package br.com.avenuecode.http;

import com.fasterxml.jackson.annotation.JsonManagedReference;

public class ImageJSON {

	private Long id;
	private String description;
	private ProductJSON product;

	public ImageJSON() {
	}

	public ImageJSON(Long id, String description, ProductJSON product) {
		this.id = id;
		this.description = description;
		this.product = product;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@JsonManagedReference
	public ProductJSON getProduct() {
		return product;
	}

	public void setProduct(ProductJSON product) {
		this.product = product;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append("id= " + id + "\n");
		sb.append("name=" + description + "\n");

		if (product != null) {
			sb.append("product.id=" + product.getId() + "\n");
			sb.append("product.description=" + product.getName() + "\n");
		}

		return sb.toString();
	}

}
