package br.com.avenuecode.http;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "image")
public class ImageXml {

	private Long id;
	private String description;
	private ProductXml product;

	public ImageXml() {
	}

	public ImageXml(Long id, String description, ProductXml product) {
		this.id = id;
		this.description = description;
		this.product = product;
	}

	@XmlElement(name = "id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@XmlElement(name = "description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@XmlElement(name = "product")
	public ProductXml getProduct() {
		return product;
	}

	public void setProduct(ProductXml product) {
		this.product = product;
	}

}
