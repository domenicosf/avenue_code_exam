package br.com.avenuecode.http;

import java.util.ArrayList;
import java.util.List;

import br.com.avenuecode.entity.Image;
import br.com.avenuecode.entity.Product;

public class ProductRelations {

	private Product product;
	private List<Image> images = new ArrayList<>();

	public ProductRelations() {

	}

	public ProductRelations(Product product, List<Image> images) {
		this.product = product;
		this.images = images;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public List<Image> getImages() {
		return images;
	}

	public void setImages(List<Image> images) {
		this.images = images;
	}

}
