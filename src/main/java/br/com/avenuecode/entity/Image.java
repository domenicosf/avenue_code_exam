package br.com.avenuecode.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "IMAGE")
@NamedQueries(value = { @NamedQuery(name = "Image.findAllImage", query = "select im from Image im"),
		@NamedQuery(name = "Image.findAllImageWithRelationship", query = "select " + "im "
				+ "from Image im join im.product p"),
		@NamedQuery(name = "Image.findImageById", query = "select im from Image im where im.id = :codigo") })
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Image {

	private Long id;
	private String description;
	private Product product;

	public Image() {
	}

	public Image(String description, Product product) {
		super();
		this.description = description;
		this.product = product;
	}

	/*
	 * Constructor used to clone image object
	 */
	public Image(Image image) {
		this.id = image.getId();
		this.description = image.getDescription();
		this.product = image.getProduct();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "IMAGE_ID")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "DESCRIPTION")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "PRODUCT_ID")
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append("id= " + id + "\n");
		sb.append("name=" + description + "\n");

		if (product != null) {
			sb.append("product.id=" + product.getId() + "\n");
			sb.append("product.description=" + product.getName() + "\n");
		}

		return sb.toString();
	}

}
