package br.com.avenuecode.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/*				"NEW br.com.avenuecode.http.ProductRelations(p, im) "*/
@Entity
@Table(name = "PRODUCT")
@NamedQueries(value = { @NamedQuery(name = "Product.findAllProduct", query = "select p from Product p"),
		@NamedQuery(name = "Product.findAllProductWithRelationship", query = "select p " + "from Product p join p.images im"),
		@NamedQuery(name = "Product.findProductById", query = "select p from Product p where p.id = :codigo"),
		@NamedQuery(name = "Product.findProductByIdWithRelationship", query = "select p "
				+ "from Product p join p.images im " + "where p.id = :codigo"),
		@NamedQuery(name = "Product.findChildrenByProductId", query = "select c "
				+ "from Product p join p.children c where p.id = :codigo"),
		@NamedQuery(name = "Product.findImagesByProductId", query = "select im "
				+ "from Product p join p.images im where p.id = :codigo") })
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Product {

	private Long id;
	private String name;
	private Product parent;
	private List<Product> children = new ArrayList<Product>();
	private List<Image> images = new ArrayList<Image>();

	public Product() {
	}

	public Product(String name, Product parent, List<Product> children, List<Image> images) {
		super();
		this.name = name;
		this.parent = parent;
		this.children = children;
		this.images = images;
	}

	/*
	 * Constructor used to clone objects
	 */
	public Product(Product product) {
		this.id = product.getId();
		this.name = product.getName();
		this.parent = product.getParent();
		this.children = product.getChildren();
		this.images = product.getImages();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "PRODUCT_ID")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "PRODUCT_NAME")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "PARENT_ID")
	public Product getParent() {
		return parent;
	}

	public void setParent(Product parent) {
		this.parent = parent;
	}

	@OneToMany(mappedBy = "parent", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JsonIgnore
	public List<Product> getChildren() {
		return children;
	}

	public void setChildren(List<Product> children) {
		this.children = children;
	}

	@OneToMany(mappedBy = "product", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JsonIgnore
	public List<Image> getImages() {
		return images;
	}

	public void setImages(List<Image> images) {
		this.images = images;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append("id= " + id + "\n");
		sb.append("name=" + name + "\n");

		if (parent != null) {
			sb.append("parent.id=" + parent.getId() + "\n");
			sb.append("parent.name=" + parent.getName() + "\n");
		}

		if ((children != null) && (children.size() > 0)) {
			for (Product child : children) {
				sb.append("child.id=" + child.getId() + "\n");
				sb.append("child.name=" + child.getName() + "\n");
			}
		}

		if ((images != null) && (images.size() > 0)) {
			for (Image image : images) {
				sb.append("image.id=" + image.getId() + "\n");
				sb.append("image.name=" + image.getDescription() + "\n");
			}
		}
		return sb.toString();
	}

}
