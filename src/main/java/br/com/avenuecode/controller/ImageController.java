package br.com.avenuecode.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.avenuecode.dao.ImageDAO;
import br.com.avenuecode.entity.Image;

/*
 * This class will expose the methods to access and modify product entity
 */
@Path("/image")
public class ImageController {

	private final ImageDAO imageDAO = new ImageDAO();

	/*
	 * This method create a new image entity on database
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON + "; charset=UTF-8")
	@Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
	@Path("/create")
	public Response create(Image imageJSON) {
		Image imageEntity = null;
		try {

			imageEntity = new Image(imageJSON);

			imageDAO.save(imageEntity);

			return Response.ok(imageEntity).build();

		} catch (Exception e) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	/*
	 * This method update an existing image entity in the database
	 */
	@PUT
	@Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
	@Consumes(MediaType.APPLICATION_JSON + "; charset=UTF-8")
	@Path("/update")
	public Response update(Image imageJSON) {

		Image imageEntity = null;

		try {
			imageEntity = new Image(imageJSON);

			imageDAO.update(imageEntity);

			return Response.ok(imageEntity).build();

		} catch (Exception e) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	/*
	 * This method recover all the image entities stored in the database
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
	@Path("/listAllImages")
	public Response findAllImages() {

		List<Image> images = imageDAO.getAllImage();

		if (images != null) {
			return Response.ok(images).build();
		} else {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	/*
	 * This method recover all the image entities and their relations stored
	 * into the database
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
	@Path("/listAllImagesWithRelationship")
	public Response findAllImagesWithRelationship() {

		List<Image> images = imageDAO.getAllImageWithRelationship();

		if (images != null) {
			return Response.ok(images).build();
		} else {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	/*
	 * This method return a specific image entity based on id
	 */
	@SuppressWarnings("unused")
	@GET
	@Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
	@Path("/getImage/{id}")
	public Response getImage(@PathParam("id") Long id) {
		Image image = imageDAO.getImageById(id);
		System.out.println(image.toString());
		if (image != null)
			return Response.ok(image).build();
		else
			return Response.status(Response.Status.NO_CONTENT).build();
	}

	/*
	 * This method removes a specific image entity stored into the database
	 */
	@DELETE
	@Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
	@Path("/delete/{id}")
	public Response delete(@PathParam("id") Long id) {
		try {
			imageDAO.deleteImageById(id);

			return Response.status(Response.Status.OK).build();
		} catch (Exception e) {
			return Response.status(Response.Status.NO_CONTENT).build();
		}
	}

}
