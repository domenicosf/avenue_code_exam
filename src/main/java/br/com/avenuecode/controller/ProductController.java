package br.com.avenuecode.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.avenuecode.dao.ProductDAO;
import br.com.avenuecode.entity.Image;
import br.com.avenuecode.entity.Product;

@Path("/product")
public class ProductController {

	private final ProductDAO productDAO = new ProductDAO();

	/*
	 * This method store a new product in the database
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON + "; charset=UTF-8")
	@Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
	@Path("/create")
	public Response create(Product productJSON) {
		Product productEntity = null;
		try {

			productEntity = new Product(productJSON);

			productDAO.save(productEntity);

			return Response.ok(productEntity).build();

		} catch (Exception e) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	/*
	 * This method update a specific product stored in the database
	 */
	@PUT
	@Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
	@Consumes(MediaType.APPLICATION_JSON + "; charset=UTF-8")
	@Path("/update")
	public Response update(Product productJSON) {

		Product productEntity = null;

		try {
			productEntity = new Product(productJSON);

			productDAO.update(productEntity);

			return Response.ok(productEntity).build();

		} catch (Exception e) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	/*
	 * This method list all products without their relations
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
	@Path("/listAllProducts")
	public Response findAllImages() {

		List<Product> products = productDAO.getAllProduct();

		if (products != null) {
			return Response.ok(products).build();
		} else {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	/*
	 * This methods returns all the products which have relationship and their
	 * relations
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
	@Path("/listAllProductsWithRelationship")
	public Response findAllProductsWithRelationship() {

		List<Product> products = productDAO.getAllProductWithRelationship();

		if (products != null) {
			return Response.ok(products).build();
		} else {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	/*
	 * This method returns all the children products based on product id
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
	@Path("/listAllChildrenByProduct/{id}")
	public Response findChildrenByProductId(@PathParam("id") Long id) {

		List<Product> children = productDAO.getChildrenByProductId(id);

		if (children != null) {
			return Response.ok(children).build();
		} else {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	/*
	 * This method return all the images attached to one product entity
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
	@Path("/listAllImagesByProduct/{id}")
	public Response findImagesByProductId(@PathParam("id") Long id) {

		List<Image> images = productDAO.getImagesByProductId(id);

		if (images != null) {
			return Response.ok(images).build();
		} else {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	/*
	 * This method return the entity which has the id specified in the url
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
	@Path("/getProduct/{id}")
	public Response getProduct(@PathParam("id") Long id) {

		Product product = productDAO.getProductById(id);

		if (product != null)
			return Response.ok(product).build();
		return Response.status(Response.Status.NO_CONTENT).build();
	}

	/*
	 * This method return one specific product and all its relations
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
	@Path("/getProductWithRelationship/{id}")
	public Response getProductWithRelationship(@PathParam("id") Long id) {

		Product product = productDAO.getProductByIdWithRelationship(id);

		if (product != null)
			return Response.ok(product).build();
		return Response.status(Response.Status.NO_CONTENT).build();
	}

	/*
	 * This method allow one entity remotion
	 */
	@DELETE
	@Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
	@Path("/delete/{id}")
	public Response delete(@PathParam("id") Long id) {
		try {
			productDAO.deleteProductById(id);

			return Response.status(Response.Status.OK).build();
		} catch (Exception e) {
			return Response.status(Response.Status.NO_CONTENT).build();
		}
	}

}
