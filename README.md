# README #

This Document will describe how put this project up and running!

### What is this repository for? ###

* Quick summary
* Version 1.0
* [Domenico Schettini Filho](https://bitbucket.org/domenicosf/avenue_code_exam/)

### How do I get set up? ###

# * Summary of set up #

To run this project all you need is to pull it using git and import in the Eclipse Framework as a Maven project.

All the dependencies were described in the Maven pom file.

# * Configuration #
First thing use git clone the project in your local machine. You can choose your usual workspace to import the project. Update the project dependencies right clicking the project name and going into the option: Maven->Update Project... 
After  build the project with "clean install" options under Maven Build in Run Configurations.

For the last step you must publish the project into a Application server like Tomcat, Glassfish and WebSphere. I recommend the use of Tomcat, because the project was tested in this application server.

# * Dependencies #
Localized on pom file in the root folder of the project.

# * Database configuration #
The database used in the project was MySQL and the configurations was defined in the persistence.xml file localized under src/main/resources/META-INF folder.
The password and the user are configured in the persistence.xml file (the default uses were root and root).

# * How to run tests #
Before run the tests you must be sure that all the steps of "Summary of set up" section were done specially the last one.
After that you can open the MySQL Workbench that follow the MySQL installation package and create a database schema named avenuecode_db. This database must be created before run any of the tests.

For run the tests I used the SOAPUI program. You must follow the bellow steps in order to use SOAPUI to run the tests:

Put click on the REST icon and put the URL to access the project. For example put the following URL: http://localhost:8080/avenue_code_exam/rest/product/

Select one of http verbs (POST, PUT, GET, DELETE) and fill the end of the resource url with the corresponding operation found in the Product and Image controllers.

*ProductController Operations:*
To access the product resource the following URL must be used: http://localhost:8080/avenue_code_exam/product/
The operations are:
**
"/create"**
This operation store a new product in the database. The operation wait for a JSON Object like that:
{
  "id" : null,
  "name" : "Letuce",
  "parent" : null
}

**"/update"**
This method update a specific product stored in the database. A JSON Object must be send in the request.
JSON Update example:
{
  "id" : 1,
  "name" : "Apple",
  "parent" : null
}

**"/listAllProducts"**
This method list all products without their relations. The products are listed as JSON objects.

**"/listAllProductsWithRelationship"**
This methods returns all the products which have relationship. The product are listed as JSON objects.

**"/listAllChildrenByProduct/id"**
This method returns all the children products based on product id. The id is the primary key of the product wanted. The output is returned as JSON object.

**"/listAllImagesByProduct/id"**
This method return all the images attached to one product entity

**"/getProduct/id"**
This method return the entity which has the id specified in the url. The id is the primary key of the product wanted. The output is returned as JSON object.

**"/getProductWithRelationship/id"**
This method return one specific product and all its relations. The id is the primary key of the product wanted. The output is returned as JSON object.

**"/delete/id"**
This method allow one product entity removal. The id is the primary key of the product wanted. The output is returned as JSON object.
"/delete/id"


*ImageController Operations:*
To access the image resource the following URL must be used: http://localhost:8080/avenue_code_exam/image/
The operations are:

**"/create"**
This operation store a new image entity in the database. The operation wait for a JSON Object like that:
{
  "id" : null,
  "description" : "Alimento usado em saladas.",
  "product" : null
}

**"/update"**
This method update a specific image entity stored in the database. A JSON Object must be send in the request.
JSON Update example:
{
  "id" : 1,
  "description" : "Alimento usado em saladas de fruta.",
  "product" : null
}

**"/listAllImages"**
This method list all images without their relations. The images are listed as JSON objects.

**"/listAllImagesWithRelationship"**
This methods returns all the images which have relationship. The images are listed as JSON objects.

**"/getImage/id"**
This method return the entity which has the id specified in the url. The id is the primary key of the image entity wanted. The output is returned as JSON object.

**"/delete/id"**
This method allow one image entity removal. The id is the primary key of the image entity wanted. The output is returned as JSON object.